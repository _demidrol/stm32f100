#include"stm32f10x.h"

void MCO_enable()
{
  RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;// + RCC_APB2ENR_AFIOEN;
  
  GPIOA->CRH = GPIO_CRH_MODE8;
  GPIOA->CRH &= ~GPIO_CRH_CNF8;
  GPIOA->CRH |= GPIO_CRH_CNF8_1; 
  
  RCC->CFGR |= RCC_CFGR_MCO_HSI; //clock source
}
