//#include "includes.h"
//#include "atomizer.h"
//#include "config.h"
//#include "blight.h"

#include "includes.h"

#define TMR_FREQ_ATOMIZER         18000000

#define APB1_FREQ_DISIRED     36000000
#define APB2_FREQ_DISIRED     72000000

#define nCH_TIM_atomizer        3
#define nTIM_atomizer           3  //
#define TIMER_ATOMIZER            concatAB(TIM, nTIM_atomizer)
#define PIN_MODE_TIM_OC                         AF_PP // Output compare channel x

#define PIN_TIM3_CH3            B, 0

// расчет значения предделителя таймера, т.е. регистра TIMx_CNT в зависимости от желаемой частоты 
// в даташите написано диапазон PSC от 1 до 65536. Поэтому -1 убрал
#if nTIM_atomizer == 1
  #define PCS_CALCUL_ATOMIZER(f) ( APB2_FREQ_DISIRED / f - 1 )
#else
  #define PCS_CALCUL_ATOMIZER(f) ( APB1_FREQ_DISIRED / f - 1 )
#endif

u16 atomizerPeriod;
u8 atomizerEnable = 0;





void AtomizerEnable()
{
  GPIO_InitTypeDef GPIO_InitStruct;
  #define GPIO_CONF1(port, pin, mode, speed)     {                \
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIO##port, ENABLE);    \
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_##pin;                    \
    GPIO_InitStruct.GPIO_Mode =  GPIO_Mode_##mode;                \
      GPIO_InitStruct.GPIO_Speed = GPIO_Speed_##speed;            \
    GPIO_Init(GPIO##port, &GPIO_InitStruct);                      \
  }
  #define GPIO_CONF(port_pin, mode, speed)   \
    GPIO_CONF1(port_pin, mode, speed)
  GPIO_CONF(concatABCD(PIN_TIM, nTIM_atomizer, _CH, nCH_TIM_atomizer), PIN_MODE_TIM_OC, 50MHz);                    

  //Log0("Атомайзер включен\n\r");
  //taskDISABLE_INTERRUPTS();
  //atomizerEnable = 1;
  //if(config.atomizer.period == PERIOD_30) atomizerPeriod = 30;
  //else atomizerPeriod = 60 * config.atomizer.period;
  //atomizerPeriod *= 60; // перевести в секунды
  //taskENABLE_INTERRUPTS();
  TIM_Cmd(TIMER_ATOMIZER, ENABLE);
  //AtomizerLedCtrl(1<<config.atomizer.period);
  //BlightEnable();
}


/*
void AtomizerDisable()
{
  GPIO_InitTypeDef GPIO_InitStruct;
  #define GPIO_CONF1(port, pin, mode, speed)     {                \
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIO##port, ENABLE);    \
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_##pin;                    \
    GPIO_InitStruct.GPIO_Mode =  GPIO_Mode_##mode;                \
      GPIO_InitStruct.GPIO_Speed = GPIO_Speed_##speed;            \
    GPIO_Init(GPIO##port, &GPIO_InitStruct);                      \
  }
  #define GPIO_CONF(port_pin, mode, speed)   \
    GPIO_CONF1(port_pin, mode, speed)
  GPIO_CONF(concatABCD(PIN_TIM, nTIM_atomizer, _CH, nCH_TIM_atomizer), Out_PP, 50MHz);
  Pset(concatABCD(PIN_TIM, nTIM_atomizer, _CH, nCH_TIM_atomizer));
  
  Log0("Атомайзер вЫключен\n\r");
  taskDISABLE_INTERRUPTS();
  atomizerEnable = 0;
  atomizerPeriod = 0;
  //taskENABLE_INTERRUPTS();
  TIM_Cmd(TIMER_ATOMIZER, DISABLE);
  AtomizerLedCtrl(0);
  BlightDisable();
}
*/

// настройка таймера атомайзера
void AtomizerInit(void)
{
  TIM_OCInitTypeDef ocCfg;
  /* TIMn clock enable */
#if nTIM_atomizer == 1
  RCC_APB2PeriphClockCmd(concatAB(RCC_APB2Periph_TIM, nTIM_atomizer), ENABLE);
#else
  RCC_APB1PeriphClockCmd(concatAB(RCC_APB1Periph_TIM, nTIM_atomizer), ENABLE);
#endif
  
  TIM_TimeBaseInitTypeDef timCfg;
  TIM_TimeBaseStructInit(&timCfg);
  timCfg.TIM_Prescaler = PCS_CALCUL_ATOMIZER(TMR_FREQ_ATOMIZER);
  timCfg.TIM_CounterMode = TIM_CounterMode_Up;
  timCfg.TIM_Period = 20;
  timCfg.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInit(TIMER_ATOMIZER, &timCfg);
  
  TIM_OCStructInit(&ocCfg);
  ocCfg.TIM_OCMode = TIM_OCMode_PWM1;
  ocCfg.TIM_OutputState = TIM_OutputState_Enable;
  ocCfg.TIM_Pulse =  10;
  ocCfg.TIM_OCPolarity = TIM_OCPolarity_High;
  concatABC(TIM_OC, nCH_TIM_atomizer, Init)(TIMER_ATOMIZER, &ocCfg);
  
  AtomizerEnable();
}

