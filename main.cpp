#include "stm32f10x.h"
//#include "atomaizer.h"
#include <stdint.h>

#include "rcc.h"

int main()
{

  SetSysClockTo64MHz_HSI(); //Частота тактирования 24 МГц
  
  //настраиваеи светодиод
  RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
  GPIOC->CRL |= GPIO_CRL_MODE7; //выход 50мгц
  GPIOC->CRL &= ~(GPIO_CRL_CNF7); // двухтактный выход 
  
  //SysTick_Config(12000000-1); //24 бита 05 sec
  //SysTick_Config(18);
  
  SysTick_Config(249);
  
  //AtomizerInit();

  
  while (1)
  {

  }
}

/*
extern "C" void TIM2_IRQHandler (void)
{
  GPIOC->ODR ^= GPIO_ODR_ODR8; // led on 
}
*/
extern "C" void SysTick_Handler(void) 
{
  GPIOC->ODR ^= GPIO_ODR_ODR7; // led on  
}
