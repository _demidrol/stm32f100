#include "stm32f10x.h"
#include "rcc.h"

void Timer2_Init (void)
{
  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  
  NVIC_EnableIRQ(TIM2_IRQn); 
  
  WRITE_REG(TIM2->PSC, 16); //значение коэффициента деления предделителя
  
  WRITE_REG(TIM2->ARR, 65535);  //число до которого считает счетчик
  
  SET_BIT(TIM2->DIER, TIM_DIER_UIE); //включаем прерывание от таймера
  
  SET_BIT(TIM2->CR1, TIM_CR1_CEN); //запускаем таймер
}

/*
TIM_CR1

Рассмотрим отдельные биты данного регистра:
Биты 9:8: CKD (Clock division) – настройки делителя
00: tDTS = tCK_INT
01: tDTS = 2 * tCK_INT
10: tDTS = 4 * tCK_INT
11: Зарезервировано.
CK_INT – частота тактирования таймера, соответственно, tCK_INT – время (или период) одного такта.

Бит 7: ARPE (Auto-reload preload enable) – бит включения предзагрузки регистра
0 – у регистра TIMx_ARR буферизация отключена,
1 – у регистра TIMx_ARR буферизация включена.

Биты 6:5: CMS (Center-aligned mode selection) – выбор режима с выравниванием по краю или одного из режимов с выравниванием по центру
00: режим с выравниванием по краю; счёт ведётся вверх или вниз (режим суммирующего или вычитающего счётчика) в зависимости от значения бита направления DIR,
01: режим 1 с выравниванием по центру; счёт ведётся вверх и вниз поочерёдно; но при счёте вниз происходит установка флага прерывания сравнения для каналов, которые сконфигурированы как выходы (CCxS=00 в регистре TIMx_CCMRx),
10: режим 2 с выравниванием по центру; счёт ведётся вверх и вниз поочерёдно; но при счёте вверх происходит установка флага прерывания сравнения для каналов, которые сконфигурированы как выходы (CCxS=00 в регистре TIMx_CCMRx),
11: режим 3 с выравниванием по центру; счёт ведётся вверх и вниз поочерёдно; установка флага прерывания сравнения для каналов, которые сконфигурированы как выходы (CCxS=00 в регистре TIMx_CCMRx) происходит как при счёте вверх, так и при счёте вниз.

Бит 4: DIR (Direction) – бит направления счёта
0: счёт вверх
1: счёт вниз

Бит 3: OPM (One-pulse mode) – бит режима одиночного импульса
0: при возникновении события обновления счётчик не останавливается
1: при возникновении события обновления счётчик останавливается

Бит 2: URS (Update request source) – бит источника запроса на обновление
0: к генерации прерывания обновления или запроса DMA (если разрешено) привело любое из следующих событий:
– переполнение/антипереполнение счётчика;
– установка бита UG;
– обновление, генерируемое с помощью контроллера подчинённого режима.
1: только переполнение (или обнуление при обратном счёте) привело к генерации UEV

Бит 1: UDIS (Update disable) – данный бит устанавливается и очищается программно для разрешения/запрета генерации события обновления (UEV)
0: разрешена генерация UEV; случаи, в которых генерируется UEV, определяются значением бита URS
1: генерация UEV отключена, теневые регистры сохраняют свои значения неизменными (ARR, PSC, CCRx), но счётчик и предделитель сбрасываются при установке бита UG или при получении сигнала аппаратного сброса от контроллера подчинённого режима.

Бит 0: CEN (Counter enable) – бит включения счётчика
0: счётчик выключен
1: счётчик включен
*/