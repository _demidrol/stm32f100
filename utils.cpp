#include "utils.h"

/*
void pll_init(void) {
    // сброс регистров RCC (выбирается HSI)
    RCC_DeInit();
    // включение и ожидания HSI
    RCC_HSICmd(ENABLE);
    while(!RCC_GetFlagStatus(RCC_FLAG_HSIRDY));
    // настройка PLL, множетель 16, частота 4 * 16 = 64 МГц
    RCC_PLLConfig(RCC_PLLSource_HSI_Div2,RCC_PLLMul_16);
    // включение и ожидания PLL
    RCC_PLLCmd(ENABLE);
    while(!RCC_GetFlagStatus(RCC_FLAG_PLLRDY));
    // настройка и вллючение буфера flash-памяти
    FLASH_SetLatency(FLASH_Latency_2);
    FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
    // установка предделителей шин
    RCC_HCLKConfig(RCC_SYSCLK_Div1);
    RCC_PCLK1Config(RCC_HCLK_Div2); // 36 MHz максимум!
    RCC_PCLK2Config(RCC_HCLK_Div1);
    // переключение SYSCLK на PLL
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
    while(RCC_GetSYSCLKSource()!=0x08);
    // обновление SystemCoreClock
    SystemCoreClockUpdate();
}
*/

/*
void mco_init(void) {
    RCC->APB2ENR |= RCC_APB2ENR_IOPAEN; // включаем тактирование порта А
    GPIOA->CRH &= ~GPIO_CRH_CNF8;       // режим push-pull
    GPIOA->CRH |= GPIO_CRH_CNF8_1;      // alternative output
    GPIOA->CRH &= ~GPIO_CRH_MODE8;      // max speed
    GPIOA->CRH |= GPIO_CRH_MODE8_1 | GPIO_CRH_MODE8_0; // max speed
    RCC->CFGR &= ~(RCC_CFGR_MCO);        // Обнуляем MCO
    RCC->CFGR |= RCC_CFGR_MCO_SYSCLK;    // системная шина
    //RCC->CFGR |= RCC_CFGR_MCO_PLL;       // PLL/2
}
*/

/*      //переключение диода при нажатии кнопки
      if ((GPIOA->IDR & GPIO_IDR_IDR0) == GPIO_IDR_IDR0) {
        for (int i = 0; i<50000; i++) {}; //убираем дребезг
        if ((GPIOA->IDR & GPIO_IDR_IDR0) == GPIO_IDR_IDR0) {
          GPIOC->ODR ^= GPIO_ODR_ODR8;
          while ((GPIOA->IDR & GPIO_IDR_IDR0) == GPIO_IDR_IDR0); 
        }
      }
*/