#include "rcc.h"
#include"stm32f10x.h"

void RCC_DeInit_2(void)
{
  RCC->CR |= RCC_CR_HSION; //HSI start
  while(READ_BIT(RCC->CR, RCC_CR_HSERDY == RESET)){}//while ((RCC->CR)&RCC_CR_HSERDY == RESET) {} //wait until starts HSI
  MODIFY_REG(RCC->CR, RCC_CR_HSITRIM, 0x80U); //calibration reset
  CLEAR_REG(RCC->CFGR); //clear configuration register 
  
  while (READ_BIT(RCC->CFGR, RCC_CFGR_SWS) != RESET) {} //wait for the RCC_CFGR_SWS bit to be cleared
  CLEAR_BIT(RCC->CR, RCC_CR_PLLON); //pll off
  while (READ_BIT(RCC->CR, RCC_CR_PLLRDY) != RESET) {} //wait for the pll to be off
  
  //hse disable
  CLEAR_BIT(RCC->CR, RCC_CR_HSEON | RCC_CR_CSSON);
  while (READ_BIT(RCC->CR, RCC_CR_HSERDY) != RESET) {}
  CLEAR_BIT(RCC->CR, RCC_CR_HSEBYP); //clear bit external generator use
  
  //Reset all CSR flags
  SET_BIT(RCC->CSR, RCC_CSR_RMVF); //remove reset flag
  
  //Disable all interrupts
  CLEAR_REG(RCC->CIR);
}

//----------------------------------------------------------

//Set clock 64MHz from HSI
void SetSysClockTo64MHz_HSI(void)
{
  RCC_DeInit_2();
  
  //SET_BIT(RCC->CR, RCC_CR_HSION);
  //while(READ_BIT(RCC->CR, RCC_CR_HSIRDY == RESET)) {}
  
  /*Включим буфер предварительной выборки, сначала отключив его, затем включим максимальную задержку, так как мы настраиваем максимальную частоту*/
  //Enable the Prefetch Buffer
  CLEAR_BIT(FLASH->ACR, FLASH_ACR_PRFTBE);
  SET_BIT(FLASH->ACR, FLASH_ACR_PRFTBE);
  MODIFY_REG(FLASH->ACR, FLASH_ACR_LATENCY, FLASH_ACR_LATENCY_2);
  
  //Сейчас Pll будет работать на частоте 4 MHz (8/2)
  
  //частотта тактирования SYSCKL максимальная (PLL/1)
  MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, RCC_CFGR_HPRE_DIV1);
  
  //APB2 тактируется с максимальной частотой (PLL/1)
  MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE2, RCC_CFGR_PPRE2_DIV1);
  
  //!!!!!!!!!!!!!!!!!!!!!!!!!максимальная частота тактирования шины APB1  36MHz (PLL/2) (64/2)
  MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE1, RCC_CFGR_PPRE1_DIV1);
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  //множитель pll x16, RCC_CFGR_PLLSRC сбрасываем, т. к. источник тактирования HSI/2
  MODIFY_REG(RCC->CFGR, /*RCC_CFGR_PLLSRC | */RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMULL,
               RCC_CFGR_PLLMULL14); //для изменения частоты тактирования нужно изменить множитель
  
  //включаем PLL, ждем готовности PLL
  SET_BIT(RCC->CR, RCC_CR_PLLON);
  while(READ_BIT(RCC->CR, RCC_CR_PLLRDY) != (RCC_CR_PLLRDY)) {}
  
  //выибираем PLL как источник тактирования, ждем когда завершится переключение источника тактирования
  MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, RCC_CFGR_SW_PLL);
  while(READ_BIT(RCC->CFGR, RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) {}
}

/*
void pll_init(void) {
    // сброс регистров RCC (выбирается HSI)
    RCC_DeInit();
    // включение и ожидания HSI
    RCC_HSICmd(ENABLE);
    while(!RCC_GetFlagStatus(RCC_FLAG_HSIRDY));
    // настройка PLL, множетель 16, частота 4 * 16 = 64 МГц
    RCC_PLLConfig(RCC_PLLSource_HSI_Div2,RCC_PLLMul_16);
    // включение и ожидания PLL
    RCC_PLLCmd(ENABLE);
    while(!RCC_GetFlagStatus(RCC_FLAG_PLLRDY));
    // настройка и вллючение буфера flash-памяти
    FLASH_SetLatency(FLASH_Latency_2);
    FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
    // установка предделителей шин
    RCC_HCLKConfig(RCC_SYSCLK_Div1);
    RCC_PCLK1Config(RCC_HCLK_Div2); // 36 MHz максимум!
    RCC_PCLK2Config(RCC_HCLK_Div1);
    // переключение SYSCLK на PLL
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
    while(RCC_GetSYSCLKSource()!=0x08);
    // обновление SystemCoreClock
    SystemCoreClockUpdate();
}
*/


/*
void RCC_DeInit_2(void)
{
  SET_BIT(RCC->CR, RCC_CR_HSION);
  while(READ_BIT(RCC->CR, RCC_CR_HSIRDY == RESET)) {}
  MODIFY_REG(RCC->CR, RCC_CR_HSITRIM, 0x80U);
  CLEAR_REG(RCC->CFGR);
  while (READ_BIT(RCC->CFGR, RCC_CFGR_SWS) != RESET) {}
  CLEAR_BIT(RCC->CR, RCC_CR_PLLON);
  while (READ_BIT(RCC->CR, RCC_CR_PLLRDY) != RESET) {}
  CLEAR_BIT(RCC->CR, RCC_CR_HSEON | RCC_CR_CSSON);
  while (READ_BIT(RCC->CR, RCC_CR_HSERDY) != RESET) {}
  CLEAR_BIT(RCC->CR, RCC_CR_HSEBYP);
  //Reset all CSR flags
  SET_BIT(RCC->CSR, RCC_CSR_RMVF);
}
*/


/*

RCC CFGR

MCO (Microcontroller clock output): битовое поле назначения вывода MCO. Данные биты устанавливаются и сбрасываются программно
0xx – Не используется (не тактируется)
100 – Совпадает с частотой тактирования системной шины (SYSCLK)
101 – Подключен к HSI
110 – Подключен к HSE
111 – Тактируется с частотой PLLCLK / 2

USBPRE (USB prescaler): включение предделителя для USB
0 – PLLCLK / 1.5
1 – PLLCLK

PLLMUL (PLL multiplication factor): данное битовое поле задаёт коэффициент умножения частоты для PLL
0000 – PLL input clock x 2
0001 – PLL input clock x 3
0010 – PLL input clock x 4
0011 – PLL input clock x 5
0100 – PLL input clock x 6
0101 – PLL input clock x 7
0110 – PLL input clock x 8
0111 – PLL input clock x 9
1000 – PLL input clock x 10
1001 – PLL input clock x 11
1010 – PLL input clock x 12
1011 – PLL input clock x 13
1100 – PLL input clock x 14
1101 – PLL input clock x 15
1110 – PLL input clock x 16
1111 – PLL input clock x 16

PLLXTPRE (HSE divider for PLL entry): предделитель перед PLL
0 – не используется
1 – частота HSE перед подачей в PLL делится на 2.

PLLSRC (PLL entry clock source): используемый вход мультиплексора входа PLL
0 – частота HSI, разделенная на 2
1 – частота HSE.

ADCPRE (ADC prescaler): данное битовое поле определяет коэффициент деления для АЦП
00 – PCLK2 / 2
01 – PCLK2 / 4
10 – PCLK2 / 6
11 – PCLK2 / 8

PPRE2 (APB high-speed prescaler (APB2)): данное битовое поле определяет коэффициент деления для APB2
0xx – HCLK без деления
100 – HCLK / 2
101 – HCLK / 4
110 – HCLK / 8
111 – HCLK / 16

APB (low-speed prescaler (APB1)): данное битовое поле определяет коэффициент деления для APB1
0xx – HCLK без деления
100 – HCLK / 2
101 – HCLK / 4
110 – HCLK / 8
111 – HCLK / 16

HPRE (AHB prescaler): данное битовое поле определяет коэффициент деления для AHB
0xxx – SYSCLK без деления
1000 – SYSCLK / 2
1001 – SYSCLK / 4
1010 – SYSCLK / 8
1011 – SYSCLK / 16
1100 – SYSCLK / 32
1101 – SYSCLK / 64
1110 – SYSCLK / 128
1111 – SYSCLK / 256.

SWS (System clock switch status): данное битовое поле используется только для чтения и показывает, какой источник используется для системного тактирования, следовательно, его биты устанавливаются и сбрасываются аппаратно
00 – HSI
01 – HSE
10 – для тактирования используется PLL
11 – не используется.

SW (System clock switch): с помощью битов данного поля можно выбрать источник для системного тактирования
00 – HSI
01 – HSE
10 – PLL
11 – не используется.
*/