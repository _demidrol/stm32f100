#ifndef INCLUDES_H
#define INCLUDES_H

#include <stdio.h>
#include <string.h>
//#include <stdarg.h>
#include <stdbool.h>

//#include "FreeRTOS.h"
//#include "task.h"
//#include "queue.h"
//#include "system_stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
//#include "stm32f10x_it.h"
#include "stm32f10x_crc.h"
#include "stm32f10x_tim.h"

#define LOG_COLOR //௤妰章 򢥲 쯣

//#define LOG0    // 
//#define LOG1    // 
//#define LOG2    // config
//#define LOG3    // 
//#define LOG4

//#include "log.h"
//#include "bsp.h"

#define sysTimer        xTaskGetTickCount()
#define ms2tick(ms)     ( ms / portTICK_RATE_MS )
#define OSTimeDly(ms)   vTaskDelay(ms2tick(ms))      // 衤汦렠⡲髠񠮯池򨮭먍

#define SemPend(sem)            xSemaphoreTake(sem, portMAX_DELAY)
#define SemPendTout(sem, tout)  xSemaphoreTake(sem, tout)
#define SemPost                 xSemaphoreGive
#define SemPostIsr              xSemaphoreGiveFromISR

#define LSI_FREQ        40000UL // [Hz] 󠱲ﳠ LSI
#define WDT_PERIOD      2000    // [ms] র鯤 񰠡᳻㡭鿠񲮰痢㯣᪬池

#define WDT_INIT        // 鮨򨠫騠򨿠񲮰痢㯣᪬池


#define concatAB_(a, b) a##b
#define concatABC_(a, b, c) a##b##c
#define concatABCD_(a, b, c, d) a##b##c##d
#define concatABCDE_(a, b, c, d, e) a##b##c##d##e
#define concatAB(a, b) concatAB_(a, b)
#define concatABC(a, b, c) concatABC_(a, b, c)
#define concatABCD(a, b, c, d) concatABCD_(a, b, c, d)
#define concatABCDE(a, b, c, d, e) concatABCDE_(a, b, c, d, e)

//#define bool _Bool
//enum {false, true};

#define delay_ms        OSTimeDly
//#define delay_mcs(mcs)  for(u32 i = 0; i < mcs*7; i++);


#define ncell(a) ( sizeof(a) / sizeof(a[0]) )

typedef unsigned long long u64;

enum { STAT_OK, STAT_ERR };

// http://arduino.on.kg/seven-segment-generator

#endif  // INCLUDES_H