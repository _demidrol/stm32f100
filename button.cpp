#include "stm32f10x.h"
#include "button.h"

void Button_interrupt (void)
{
  RCC->APB2ENR |= RCC_APB2ENR_IOPCEN + RCC_APB2ENR_IOPAEN; //включаем тактирование НЕОБХОДИМОГО ПОРТА 
  
  //настраиваем светодиод
  GPIOC->CRH |= GPIO_CRH_MODE8; //выход 50мгц
  GPIOC->CRH &= ~(GPIO_CRH_CNF8); // двухтактный выход 
  
  
  //Настраиваем необходимую кнопку как пдавающий вход  (A0)
  GPIOA->CRL &= ~GPIO_CRL_MODE0; //вход
  GPIOA->CRL |= GPIO_CRL_CNF0_1; //CNF 01 - плавающий
  GPIOA->CRL &= ~GPIO_CRL_CNF0_0;
  
  
  EXTI->IMR |= EXTI_IMR_MR0;   //локально разрешили прерывание
  EXTI->FTSR &= ~EXTI_FTSR_TR0; // замаскировали прерывание по спадающему фронту
  EXTI->RTSR |= EXTI_RTSR_TR0; //разрешили прерывание по нарастающему фронту
  
  NVIC_EnableIRQ(EXTI0_IRQn); //включили прерывание модуля EXTI0 в NVIC
}

extern "C" void EXTI0_IRQHandler(void) 
{    
    EXTI->PR |= EXTI_PR_PR0; //Здесь необходимо сбрасывать флаг пойманного прерывания путем записи в него единицу

//    if (flag) {
//        GPIOC->ODR |= GPIO_ODR_ODR8; // led on
//        flag = 0;
//    } else {
//        GPIOC->ODR &= ~GPIO_ODR_ODR8; // led off
//        flag = 1;
//    }
    GPIOC->ODR ^= GPIO_ODR_ODR8; // led toggle 

}